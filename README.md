# Gralyn Core
This is still pretty heavily under construction, but it is functional. If you find any bugs, let me know.

## Dependencies:
You must have Python 3.6.5 or newer and the discord.py rewrite installed. The legacy version of discord.py will *not* work, you *must* use the rewrite. The rewrite can be installed from pip:
```
pip install -U git+https://github.com/Rapptz/discord.py@rewrite#egg=discord.py[voice]
```

## Plugins
Plugins are written via the discord.py rewrite extensions framework, which is not yet documented. I will link to it here once it is, but for now you can use these extensions I've written as templates for your own plugins.


To install a plugin, simply place it in the plugins folder

### Plugins (these repos are the same)
GitLab: https://gitlab.com/GNUGradyn/gralyn-core-plugins (preferred)  
Personal Git: https://git.gradyn.com/GNUGradyn/gralyn-core-plugins

## Config values
Configurable things you can configure with the set command (I'll probably have a better way to set these in the future)

**dm-help** [True/False] - *Sets if help is sent in a DM or where the command was invoked (True for DM)*  