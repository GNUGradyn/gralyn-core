print("Gralyn Core Copyright Gradyn Wursten 2018-2020")
from colorama import init, Fore, Back, Style
init()
import os
import json
import discord
from discord.ext import commands
from discord.ext.commands import Cog
import traceback
import sqlite3
import sys
sys.path.append(os.path.dirname(__file__)) #Temporary patch for ModuleNotFoundError when using redistributable python

@staticmethod
def read(key):
    t=(key,)
    if len(db_cursor.execute("SELECT * FROM misc_data where key=?", t).fetchall()) == 0:
        return None
    else:
        return db_cursor.execute("SELECT * FROM misc_data where key=?", t).fetchone()[1]
@staticmethod
def write(key, value):
    t=(key,)
    if not len(db_cursor.execute("SELECT * FROM misc_data where key=?", t).fetchall()) == 0:
        db_cursor.execute("DELETE FROM misc_data WHERE key=?;", t)
    t = (key, value)
    db_cursor.execute("INSERT INTO misc_data VALUES (?,?)", t)
@staticmethod
def delete(key):
    t=(key,)
    db_cursor.execute("DELETE FROM misc_data WHERE key=?;", t)
def commit(*args):
    db.commit()
    raw_config.seek(0)
    raw_config.write(json.dumps(config))
    raw_config.truncate()
    info("Saved")
def warning(text):
    print(Style.BRIGHT + Fore.WHITE + '[' + Fore.MAGENTA + '!' + Fore.WHITE + '] ' + Style.RESET_ALL + text)
def info(text):
    print(Style.BRIGHT + Fore.WHITE + '[' + Fore.BLUE + 'I' + Fore.WHITE + '] ' + Style.RESET_ALL + text)
def denied(*args):
    if denied not in config:
        warning("Denied not set! Setting default..")
        config["denied"] = "You are not allowed to perform that command!"
        commit()
    return config["denied"]
def RepInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

if not os.path.exists('config.json'):
    warning("config.json does not exist! Creating it..")
    with open('config.json', 'w+') as f:
        f.write('{}')
if not os.path.exists('plugins'):
    warning("Plugins folder does not exist! Creating it..")
    os.makedirs('plugins')
raw_config = open('config.json', 'r+')
config = json.load(raw_config)
info("Loaded config.json")
if "token" not in config:
    warning('Token not set!')
    config["token"] = input("Enter your token from discordapi.com here: ")
if "prefix" not in config:
    warning("Prefix not set!")
    config["prefix"] = input("Enter a prefix here (if you wanted help to be /help, you'd type / here): ")
if "dm-help" not in config:
    config["dm-help"] = True
if "chat-error-handling" not in config:
    config["chat-error-handling"] = True
db = None
db_cursor = None
if not os.path.exists("core.db"):
    warning("Database does not exist! Creating it..")
    db = sqlite3.connect('core.db')
    db_cursor = db.cursor()
    db_cursor.execute("CREATE TABLE misc_data (key, value)")
else:
    db = sqlite3.connect('core.db')
    db_cursor = db.cursor()
info("Connected to database")
commit()

discord.ext.commands.Bot.config = config
discord.ext.commands.Bot.commit = commit
discord.ext.commands.Bot.denied = denied
discord.ext.commands.Bot.info = staticmethod(info)
discord.ext.commands.Bot.warning = staticmethod(warning)
discord.ext.commands.Bot.write = write
discord.ext.commands.Bot.read = read
discord.ext.commands.Bot.delete = delete
discord.ext.commands.Bot.db_cursor = db_cursor
discord.ext.commands.Bot.db = db
bot = discord.ext.commands.Bot(config["prefix"], formatter=None, description=None, pm_help=config["dm-help"])
class Core(commands.Cog):
    @Cog.listener()
    async def on_message(self, message):
        if message.content == f"{config['prefix']}help":
            if config["dm-help"] == True:
                await message.add_reaction('📫')
    @Cog.listener()
    async def on_ready(self):
        info("Connected to Discord successfully!")
        bot_info = await bot.application_info()
        if 'admins' not in config:
            warning(f"No admins defined! Adding {bot_info.owner.name} to admins")
            config["admins"] = list()
            config["admins"].append(int(bot_info.owner.id))
            commit()
        if len(config["admins"]) == 0:
            warning(f"No admins defined! Adding {bot_info.owner.name} to admins")
            config["admins"].append(int(bot_info.owner.id))
            commit()
    def __init__(self, bot):
      self.bot = bot
    if config["chat-error-handling"]:
        @Cog.listener()
        async def on_command_error(self, ctx, error):
            if not isinstance(error, commands.CommandNotFound):
                embed = discord.Embed(title=error.original.args[0], description="".join(traceback.format_exception(type(error.original), error.original, error.original.__traceback__)))
                embed.add_field(name="Plugin", value=self.bot.all_commands[ctx.invoked_with].module)
                embed.add_field(name="Command", value=ctx.view.buffer)
                await ctx.send(embed=embed)
    @commands.command()
    async def admin(self, ctx, command, user=None):
        """Add, list, or remove admins from the bot"""
        if ctx.message.author.id in bot.config["admins"]:
            if command == "help":
                await ctx.send("admin (add/remove/list) [user ID or full tag]")
            elif command == "list":
                    admins = list()
                    for x in bot.config["admins"]:
                        user = bot.get_user(x)
                        admins.append(f"{user.name}:{user.discriminator}")
                    await ctx.send(', '.join(admins))
            elif command == "add" or "remove":
                if user == None:
                    await ctx.send("admin (list/add/remove) [user tag/ID]")
                else:
                    id = None
                    if RepInt(user):
                        id = user
                    elif user.startswith("<@!") and user[-1:] == ">":
                        id = user[3:-1]
                    elif user.startswith("<@") and user[-1:] == ">":
                        id = user[2:-1]
                    target = bot.get_user(int(id))
                    if command == "add":
                        if target == None:
                            await ctx.send("User does not exist!")
                        else:
                            if int(id) in bot.config["admins"]:
                                await ctx.send("User is already admin!")
                            else:
                                bot.config["admins"].append(int(id))
                                commit()
                                await ctx.send(f"added {target.name}:{target.discriminator} to list of admins")
                    else:
                        if int(id) not in bot.config["admins"]:
                            await ctx.send("User is not admin!")
                        else:
                            bot.config["admins"].remove(int(id))
                            commit()
                            user = bot.get_user(int(id))
                            await ctx.send(f"removed {target.name}:{target.discriminator} from list of admins")
            else:
                await ctx.send("admin (list/add/remove) [user tag/ID]")
        else:
            await ctx.send(denied())
    @commands.command(name="info")
    async def infocmd(self, ctx):
        """Info about gralyn core"""
        await ctx.send("Gralyn Core Copyright Gradyn Wursten 2018\nSee https://gitlab.com/GNUGradyn/gralyn-core for more info")
    @commands.command()
    async def set(self, ctx, key, value):
        if ctx.author.id in config["admins"]:
            if key in config:
                if type(config[key]) == bool:
                    if value in ["true", "True"]:
                        config[key] = True
                        update = True
                    elif value in ["false", "False"]:
                        config[key] = False
                        update = True
                    else:
                        await ctx.send(f"{key} must be true or false!")
                        update = False
                else:
                    config[key] = value
                    update = True
                if update:
                    commit()
                    await ctx.send("Done - You may need to restart the bot for this to take effect")
            else:
                await ctx.send("No such key")
        else:
            await ctx.send(denied())
    @commands.command()
    async def servers(self, ctx, command, *argument):
        """List servers bot is a member of or leave a server"""
        if ctx.author.id in config["admins"]:
            if command == "list":
                guild_names = []
                for x in bot.guilds:
                    guild_names.append(x.name)
                await ctx.send(', '.join(guild_names))
            if command == "leave":
                if len(argument) == 0:
                    await ctx.send("You must specify a guild by name to leave")
                else:
                    match = False
                    for x in bot.guilds:
                        if x.name == ' '.join(argument):
                            await x.leave()
                            await ctx.message.add_reaction("✅")
                            match = True
                            break
                    if match == False:
                        await ctx.send("I'm not in that guild! Check the spelling")
        else:
            await ctx.send(denied())
info("Loading core...")
bot.add_cog(Core(bot))
plugins = os.listdir("plugins")
for load in plugins:
    if os.path.isfile("plugins/"+load):
        info(f"Loading plugins.{load.replace('.py', '')}...")
        bot.load_extension('plugins.'+load.replace('.py', ''))
info(f"Connecting to Discord...")
bot.run(config["token"])